#/usr/bin/perl

use warnings;
use strict;

use lib '.';
use Darkwoods::Game;

my $game = Darkwoods::Game->new();
$game->menu;
