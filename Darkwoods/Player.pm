package Darkwoods::Player;
use Moo;

has x      => is => 'rw', default => 0;
has y      => is => 'rw', default => 0;
has magic  => is => 'rw', default => 0;
has arrows => is => 'rw', default => 0;
has keys   => is => 'rw', default => 0;
has coins  => is => 'rw', default => 0;
has jewels => is => 'rw', default => 0;

sub move {
    my $self = shift;
    my ( $offset_x, $offset_y ) = @_;

    $self->x( $self->x + $offset_x );
    $self->y( $self->y + $offset_y );
}

sub place {
    my $self = shift;
    my ( $x, $y ) = @_;
    $self->x($x);
    $self->y($y);
}

1;
