package Darkwoods::Map;

use Moo;

use File::Slurp;

use Text::Wrap;
use Term::ANSIColor;
use Darkwoods::Player;
use Darkwoods::Enemy;

has map     => is => 'rw';
has player  => is => 'rw';
has message => is => 'rw', default => '';
has file    => is => 'rw', default => '';
has enemies => is => 'rw', default => sub { [] };

sub build_map {
    my $self = shift;

    my @map_raw_data = read_file( $self->file );

    my $mapping = {
        'R' => 99,    # Wall
        'W' => 98,    # Wall
        'V' => 97,    # Wall
        'B' => 50,    # Bush
        'T' => 51,    # Tree
        'M' => 10,    # Mana
        'A' => 11,    # Arrows
        'K' => 12,    # Key
        '$' => 13,    # Coins
        'J' => 14,    # Jewel
        'D' => 80,    # Door
    };

    my $map = [];
    my $x   = 0;
    my $y   = 0;
    foreach my $row (@map_raw_data) {
        $x = 0;
        my $col_data = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
        my @cols = split( //, $row );
        foreach my $col (@cols) {
            my $tile = $mapping->{$col} || 0;
            $col_data->[$x] = $tile;
            if ( $col eq '*' ) {
                $self->player->place( $x, $y );
            }
            elsif ( $col eq 'C' ) {
                my $enemy = Darkwoods::Enemy->new(
                    {   map       => $self,
                        player    => $self->player,
                        x         => $x,
                        y         => $y,
                        color     => 'bright_red',
                        character => 'C',
                    }
                );
                push @{ $self->enemies }, $enemy;
            }
            $x++;
        }
        $y++;
        push @$map, $col_data;
    }

    $self->map($map);

}

sub check_player_pickup {
    my $self = shift;

    my $map    = $self->map;
    my $player = $self->player;
    my $x      = $player->x;
    my $y      = $player->y;
    my $tile   = $map->[$y][$x];

    # items
    if ( $tile > 9 && $tile < 20 ) {
        if ( $tile == 10 ) {
            $self->message("You found some mana!");
            $player->magic( $player->magic + 3 );
        }
        if ( $tile == 11 ) {
            $self->message("You found some arrows!");
            $player->arrows( $player->arrows + 3 );
        }
        if ( $tile == 12 ) {
            $self->message("You found an key!");
            $player->keys( $player->keys + 1 );
        }
        if ( $tile == 13 ) {
            $self->message("You found some coins!");
            $player->coins( $player->coins + 10 );
        }
        if ( $tile == 14 ) {
            $self->message("You found a jewel!");
            $player->jewels( $player->jewels + 1 );
        }
        $map->[$y][$x] = 0;
    }
}

sub try_door {
    my $self = shift;
    my ( $x, $y ) = @_;

    my $map    = $self->map;
    my $player = $self->player;
    my $tile   = $map->[$y][$x];

    if ( $tile > 79 && $tile < 82 ) {
        if ( $player->keys > 0 ) {
            $self->message("You unlocked the door!");
            $player->keys( $player->keys - 1 );
            $map->[$y][$x] = 0;
        }
        else {
            $self->message("You need a key for the door!");
        }
    }
}

sub is_solid {
    my $self = shift;
    my ( $x, $y ) = @_;

    my $map   = $self->map;
    my $solid = 0;

    if ( $map->[$y][$x] < 50 ) {
        $solid = 1;
    }

    return $solid;
}

sub clear_tile {
    my $self = shift;
    my ( $x, $y ) = @_;
    $self->map->[$y][$x] = 0;

    for ( my $index = 0; $index < scalar @{ $self->enemies }; $index++ ) {
        my $enemy = $self->enemies->[$index];
        if ( $enemy->x == $x && $enemy->y == $y ) {
            splice( @{ $self->enemies }, $index, 1 );
            $self->message("You kill the enemy with your sword!");
            last;
        }
    }
}

sub is_breakable {
    my $self = shift;
    my ( $x, $y ) = @_;
    my $tile = $self->map->[$y][$x];
    return 1 if $tile == 50 || $tile == 0;
    return 0;
}

sub display {
    my $self   = shift;
    my $map    = $self->{map};
    my $player = $self->player;

    STDOUT->flush;

    $self->hide_cursor;

    # $self->restore_cursor;

    # clear screen
    # print "\e[2J";

    my $tiles = {
        80 => {
            character => '-',
            color     => 'white on_yellow',
        },
        99 => {
            character => ' ',
            color     => 'on_white',
        },
        98 => {
            character => '#',
            color     => 'grey10 on_grey8',
        },
        97 => {
            character => '*',
            color     => 'grey8 on_grey10',
        },
        0 => {
            character => ' ',
            color     => 'white on_grey0',
        },
        1 => {
            character => '.',
            color     => 'green on_grey0',
        },
        10 => {
            character => '*',
            color     => 'bright_red on_grey0',
        },
        11 => {
            character => 'a',
            color     => 'bright_yellow on_grey0',
        },
        12 => {
            character => 'k',
            color     => 'bright_blue on_grey0',
        },
        13 => {
            character => '$',
            color     => 'bright_yellow on_grey0',
        },
        14 => {
            character => '$',
            color     => 'bright_magenta on_grey0',
        },
        50 => {
            character => '#',
            color     => 'green on_grey0',
        },
        51 => {
            character => 'T',
            color     => 'green on_grey0',
        },
    };

    my $x = 0;
    my $y = 0;
    foreach my $row ( @{ $self->{map} } ) {
        foreach my $col (@$row) {
            my $character;
            my $color;
            if ( $x == $player->x && $y == $player->y ) {
                $character = '@';
                $color     = 'bright_white on_grey0';
            }
            else {
                $character = $tiles->{$col}->{character} || ' ';
                $color     = $tiles->{$col}->{color}     || 'white';
            }

            foreach my $enemy ( @{ $self->enemies } ) {
                if ( $enemy->x == $x && $enemy->y == $y ) {
                    $color     = $enemy->color;
                    $character = $enemy->character;
                }
            }

            print "\e[" . ( $y + 5 ) . ";" . ( $x + 5 ) . "H";
            print colored( [$color], $character, '' );
            $x++;
        }
        $y++;
        $x = 0;
    }

    if ( $self->message ) {
        print "\n" . $self->message . "\n";
        $self->message('');
    }
    else {
        print "\n" . ' ' x 40 . "\n";
    }
}

sub move_enemies {
    my $self = shift;

    foreach my $enemy ( @{ $self->enemies } ) {
        $enemy->move();
    }
}

sub hide_cursor {
    my $self = shift;
    print "\e[?25l";
}

sub show_cursor {
    my $self = shift;
    print "\e[?25h";
}

sub restore_cursor {
    my $self = shift;
    printf "\e[%dA", 30;
}

sub is_player_dead {
    my $self   = shift;
    my $player = $self->player;

    foreach my $enemy ( @{ $self->enemies } ) {
        if ( $enemy->x == $player->x && $enemy->y == $player->y ) {
            return 1;
        }
    }

    return 0;
}

1;
