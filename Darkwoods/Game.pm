package Darkwoods::Game;
use Moo;

use Darkwoods::Map;
use Term::ReadKey;
use Try::Tiny;
use Text::Wrap;
use Term::ANSIColor;
use Time::HiRes qw( usleep );

$| = 1;

sub menu {
    my $self = shift;

    my ( $wchar, $hchar, $wpixels, $hpixels ) = GetTerminalSize();

    my $offset_y = int( $hchar / 2 );
    my $offset_x = int( ( $wchar - 20 ) / 2 );

    ReadMode('cbreak');
    print "\e\033[?47h";    # save screen

    draw_menu( $offset_y, $offset_x );

    my %special_keys = (
        65 => "Up",
        66 => "Down",
        67 => "Right",
        68 => "Left",
    );

    my $error;
    try {
        my $option = 0;
        my $pos_y  = 1;
        my $quit   = 0;
        my $char;

        print "\e[" . ( $pos_y + $offset_y ) . ";" . ($offset_x) . "H";
        print colored( ['white '], '-=>', '' );
        print "\e[" . ($hchar) . ";" . ($wchar) . "H";

        while ( !$quit ) {
            $char = ReadKey(0);

            last unless defined $char;

            my $key = $special_keys{ ord($char) } || next;

            print "\e[" . ( $pos_y + $offset_y ) . ";" . ($offset_x) . "H";
            print colored( ['white'], '   ', '' );

            if ( $key eq "Down" ) {
                if ( $option < 2 ) {
                    $option++;
                    $pos_y++;
                }
            }

            if ( $key eq "Up" ) {
                if ( $option > 0 ) {
                    $option--;
                    $pos_y--;
                }
            }

            if ( $key eq "Right" ) {
                if ( $option == 2 ) {
                    select_option( $pos_y, $offset_y, $offset_x, $hchar, $wchar );
                    $quit = 1;
                }

                if ( $option == 0 ) {
                    select_option( $pos_y, $offset_y, $offset_x, $hchar, $wchar );
                    print "\033[2J";    #clear the screen
                    $self->run();
                    draw_menu( $offset_y, $offset_x );
                }
            }

            print "\e[" . ( $pos_y + $offset_y ) . ";" . ($offset_x) . "H";
            print colored( ['white'], '-=>  ', '' );
            print "\e[" . ($hchar) . ";" . ($wchar) . "H";
        }
    }
    catch {
        $error = $_;
    }
    finally {

        # clear screen
        print "\e[2J";

        # reset cursor
        print "\e[1;1H";

        # show cursor
        print "\e[?25h";

        # restore terminal
        print "\e\033[?47l";

        # display any errors
        print "$error" if $error;

        print "\033[2J";      #clear the screen
        print "\033[0;0H";    #jump to 0,0

        ReadMode 1;
    }

}

sub select_option {
    my ( $pos_y, $offset_y, $offset_x, $hchar, $wchar ) = @_;
    for ( my $z = 0; $z < 4; $z++ ) {
        print "\e[" . ( $pos_y + $offset_y ) . ";" . ($offset_x) . "H";
        print colored( ['white'], '   ', '' );
        print "\e[" . ( $pos_y + $offset_y ) . ";" . ( $offset_x - 1 + $z ) . "H";
        print colored( ['white'], ' -=>', '' );
        print "\e[" . ($hchar) . ";" . ($wchar) . "H";
        usleep(100000);
    }
    usleep(100000);
}

sub draw_menu {
    my ( $offset_y, $offset_x ) = @_;

    my ( $wchar, $hchar, $wpixels, $hpixels ) = GetTerminalSize();

    print colored( ['white on_red'], ' ', '' );

    print "\033[2J";      #clear the screen
    print "\033[0;0H";    #jump to 0,0

    my @animation = ( "n", "on", "xon", "ixon", "Nixon", );

    foreach my $x (@animation) {
        print "\e[" . ($hchar) . ";" . (1) . "H";
        print $x;

        print "\e[" . ($hchar) . ";" . ($wchar) . "H";
        usleep(100000);
    }

    @animation = (
        "                 ",
        "                 ",
        "                 ",
        "- - - - - - - - -",
        "* * * * * * * * *",
        "# # # # # # # # #",
        "D A R K W O O D S",
    );

    foreach my $x (@animation) {
        print "\e[" . ( -2 + $offset_y ) . ";" . ($offset_x) . "H";
        print $x;

        print "\e[" . ($hchar) . ";" . ($wchar) . "H";
        usleep(200000);
    }

    print "\e[" . ( -2 + $offset_y ) . ";" . ($offset_x) . "H";
    print "D A R K W O O D S";

    @animation = (
        ">",                     "->",                 "-->",                 "--->",
        "---->",                 "----->",             "------>",             "------->",
        "-------->",             "--------->",         "---------->",         "----------->",
        "------------>",         "------------->",     "-------------->",     "--------------->",
        "---------------->",     "----------------->", "----------------- >", "-----------------  +",
        "-----------------   -", "-----------------    ",
    );

    foreach my $x (@animation) {
        print "\e[" . ( -1 + $offset_y ) . ";" . ($offset_x) . "H";
        print $x;
        print "\e[" . ($hchar) . ";" . ($wchar) . "H";
        usleep(50000);
    }

    print "\e[" . ( -1 + $offset_y ) . ";" . ($offset_x) . "H";
    print "-----------------";

    print "\e[" . ( 1 + $offset_y ) . ";" . ( 5 + $offset_x ) . "H";
    print " Play Game";
    print "\e[" . ( 2 + $offset_y ) . ";" . ( 5 + $offset_x ) . "H";
    print " Load Map";
    print "\e[" . ( 3 + $offset_y ) . ";" . ( 5 + $offset_x ) . "H";
    print " Quit";
}

sub run {
    my $self = shift;
    ReadMode('cbreak');

    my $player = Darkwoods::Player->new( x => 1, y => 1 );
    my $map = Darkwoods::Map->new(
        player => $player,
        file   => './Maps/map1.dat',
    );
    $map->build_map;
    $map->display;

    my %special_keys = (
        65 => "Up",
        66 => "Down",
        67 => "Right",
        68 => "Left",
    );

    my $char = '';
    $map->display;

    while ( $char ne 'q' ) {
        $char = ReadKey(0);

        last unless defined $char;

        my $key = '';

        if ( ord($char) == 27 ) {
            $char = ReadKey(0);
            $char = ReadKey(0);
            if ( ord($char) == 49 ) {
                $key  = "Shift + ";
                $char = ReadKey(0);
                $char = ReadKey(0);
                $char = ReadKey(0);
            }
            $key .= $special_keys{ ord($char) };
        }
        else {
            $key = $char;
        }

        # Sword action
        if ( $key eq 'Shift + Down' ) {
            if ( $map->is_breakable( $player->x, $player->y + 1 ) ) {
                $map->clear_tile( $player->x, $player->y + 1 );
            }
        }
        if ( $key eq 'Shift + Up' ) {
            if ( $map->is_breakable( $player->x, $player->y - 1 ) ) {
                $map->clear_tile( $player->x, $player->y - 1 );
            }
        }
        if ( $key eq 'Shift + Left' ) {
            if ( $map->is_breakable( $player->x - 1, $player->y ) ) {
                $map->clear_tile( $player->x - 1, $player->y );
            }
        }
        if ( $key eq 'Shift + Right' ) {
            if ( $map->is_breakable( $player->x + 1, $player->y ) ) {
                $map->clear_tile( $player->x + 1, $player->y );
            }
        }

        # Movement
        my $movex = 0;
        my $movey = 0;
        if ( $key eq 'Down' )  { $movey++ }
        if ( $key eq 'Up' )    { $movey-- }
        if ( $key eq 'Left' )  { $movex-- }
        if ( $key eq 'Right' ) { $movex++ }

        my $newx = $player->x + $movex;
        my $newy = $player->y + $movey;

        $map->try_door( $newx, $newy );

        if ( $map->is_solid( $newx, $newy ) ) {
            $player->move( $movex, $movey );
        }

        $char = 'q' if $self->check_death($map);

        $map->check_player_pickup();
        $map->move_enemies();
        $map->display();

        $char = 'q' if $char ne 'q' && $self->check_death($map);
    }
}

sub check_death {
    my $self = shift;
    my ($map) = @_;

    if ( $map->is_player_dead() ) {
        $map->message("Player has died");
        $map->display();
        $self->death_screen();
        ReadKey(0);
        return 1;
    }
}

sub death_screen {
    my $self = shift;

    my ( $wchar, $hchar, $wpixels, $hpixels ) = GetTerminalSize();

    my @pos;
    my $complete = 0;
    while ( $complete < $wchar ) {
        my $min = $hchar;
        my $max = 0;
        $complete = 0;
        for ( my $x = 0; $x < $wchar; $x++ ) {
            my $y = $pos[$x] || 0;
            $y++ if ( int( rand() * 2 ) == 1 );
            $pos[$x] = $y;

            $min = $y if $y < $min;
            $max = $y if $y > $max;

            print "\e[" . ($y) . ";" . ( $x + 1 ) . "H";
            print colored( ['on_red'], ' ', '' );

            if ( $y + 1 < $hchar + 1 ) {
                my $char = int( rand() * 2 ) == 1 ? '*' : '#';
                print "\e[" . ( $y + 1 ) . ";" . ( $x + 1 ) . "H";
                print colored( ['black on_red'], $char, '' );
            }

            if ( $y + 2 < $hchar + 1 ) {
                my $char = int( rand() * 2 ) == 1 ? '.' : ',';
                print "\e[" . ( $y + 2 ) . ";" . ( $x + 1 ) . "H";
                print colored( ['red'], $char, '' );
            }

            if ( $y > $hchar + 1 ) {
                $complete++;
                next;
            }
        }

        my $text_y = int( ( ( ( $max - $min ) / 2 ) + $min ) / 2 );
        $text_y = int( $hchar / 2 ) if $text_y > int( $hchar / 2 );

        if ( ( $text_y - 1 ) > 1 ) {
            print "\e[" . ( int( $text_y - 1 ) ) . ";" . ( int( $wchar / 2 ) - 11 ) . "H";
            print colored( ['black on_red'], '               ', '' );
        }

        if ( ($text_y) > 1 ) {
            my $death_string = 'Y O U   D I E D';
            print "\e[" . ( int($text_y) ) . ";" . ( int( $wchar / 2 ) - 11 ) . "H";
            print colored( ['black on_red'], $death_string, '' );
        }

        if ( $min > int( $hchar / 2 ) ) {
            my $death_string = '---------------';
            print "\e[" . ( int( $hchar / 2 ) + 1 ) . ";" . ( int( $wchar / 2 ) - 11 ) . "H";
            print colored( ['black on_red'], $death_string, '' );
        }

        usleep(50000);
    }
}

1;
