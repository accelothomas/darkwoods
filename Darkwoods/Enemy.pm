package Darkwoods::Enemy;
use Moo;

has x         => is => 'rw', default => 0;
has y         => is => 'rw', default => 0;
has character => is => 'rw', default => 'E';
has color     => is => 'rw', default => 'red';
has movement  => is => 'rw', default => 'random';
has player    => is => 'rw';
has map       => is => 'rw';

sub place {
    my $self = shift;
    my ( $x, $y ) = @_;

    $self->x($x);
    $self->y($y);
}

sub move {
    my $self = shift;

    # get move based on movement
    my ( $ox, $oy ) = $self->random_move();

    # apply move
    my $newx = $self->x + $ox;
    my $newy = $self->y + $oy;

    if ( $self->is_valid_move( $newx, $newy ) ) {
        $self->x($newx);
        $self->y($newy);
    }
}

sub is_valid_move {
    my $self = shift;
    my ( $x, $y ) = @_;

    if ( $self->map->is_solid( $x, $y ) ) {
        foreach my $enemy ( @{ $self->map->enemies } ) {
            if ( $enemy->x == $x && $enemy->y == $y ) {
                return 0;
            }
        }
        return 1;
    }

    return 0;
}

sub random_move {
    my $self = shift;

    my $x         = 0;
    my $y         = 0;
    my $direction = int( rand(4) );

    $x-- if ( $direction == 0 );
    $x++ if ( $direction == 1 );
    $y-- if ( $direction == 2 );
    $y++ if ( $direction == 3 );

    return ( $x, $y );
}

1;
